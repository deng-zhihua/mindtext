# MindText

## Introduction

MindText is an open source text toolbox based on MindSpore.

The master branch works with **MindSPore 1.2**.


## License

This project is released under the [Apache 2.0 license](LICENSE).

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mindspore/mindtext/issues).

## Acknowledgement

MindSpore is an open source project that welcome any contribution and feedback.
We wish that the toolbox and benchmark could serve the growing research
community by providing a flexible as well as standardized toolkit to reimplement existing methods
and develop their own new semantic segmentation methods.

# Contributor
xiaoyisd(524925587@qq.com)
Norato(834184466@qq.com)
bridge(1131156501@qq.com)
ChanJiatao(chenjiatao777@163.com)
LiuHongYe(470141216@qq.com)
ChiJunlong(2869897445@qq.com)
pallidlight(965504148@qq.com)
TsaiKunzhao(562552741@qq.com)
DingMingHao(294270681@qq.com)
caijian(937747918@qq.com)
HuangZhengBo(1219326125@qq.com)
huangsonglong(2802993151@qq.com)
txzskywalker(73560446@qq.com)
chenhuamin9527(359247454@qq.com)
dongjianyu(1325895127@qq.com)
Carina233(910780586@qq.com)
LunarCM(708827045@qq.com)
xay2001(1602357810@qq.com)
xiaoyuzhau(1042958277@qq.com)
lijianming(lijianming666666@163.com)
ChenXuechen(312117408@qq.com)
jmsyyds(1535276562@qq.com)
xiaoyuzhau(1042958277@qq.com) 
ChenXuechen(312117408@qq.com) 
Hamilton北（1209233513@qq.com）
huanghonghao(373658829@qq.com)
linkaizhe(499780002@qq.com)
Divine(2538048363@qq.com)
LiangRio(2453372256@qq.com)
jamesonleong(jamesonleong@qq.com)
Liyanlong(2373515564@qq.com)
MChloe(2284161124@qq.com)
M-Zif(lucas011014@qq.com)
Rayen227(16607615570@163.com)
7aY1 (389354380@qq.com)
zhangjiadong(2628961452@qq.com)
Swing9912(843678794@qq.com)
Lee_rvr(741347759@qq.com)
Longhaolin(2916691149@qq.com)
XingXing(2407116610@qq.com)
BaYan01(1667041026@qq.com)
Yuzi(953984595@qq.com)
Liyanlong(2373515564@qq.com)
Soleil(1272134437@qq.com)

## Citation

If you find this project useful in your research, please consider citing:

```latex
@misc{mindvsion2021,
    title={{MindText}:MindSpore Vision Toolbox and Benchmark},
    author={MindText Contributors},
    howpublished = {\url{https://gitee.com/mindspore/mindtext}},
    year={2021}
}
```
